import { Component, OnInit } from '@angular/core';
import { ItemComponent } from '../../app/item/item.component'
import { ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-new-shopping-list',
  templateUrl: './new-shopping-list.component.html',
  styleUrls: ['./new-shopping-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class NewShoppingListComponent implements OnInit {

  itemList: ItemComponent[] = [];
  counter: number = 0;

  constructor() { }

  ngOnInit(): void {
  }

  addNewRow(){
    var d1 = document.getElementById("bufferrow1");
    console.log(this.counter);
    this.counter = this.counter+1;
    var newRow = '<div class="new-list-list-row"> <input class="list-input" type="text" placeholder="Item Name" id="item-name'+this.counter+'" name="item-name"> <input class="list-input" type="text" placeholder="Item Cost" id="item-cost'+this.counter+'" name="item-cost"> <input id="item-amount'+this.counter+'" type="text" value="1"/> </div>';

    // need type dropdown TODO
    d1.insertAdjacentHTML('beforeend',newRow);
  }

  submitform(){
    for(let i =0; i<= this.counter; i++){
      console.log(this.counter);
    }

  }



}
