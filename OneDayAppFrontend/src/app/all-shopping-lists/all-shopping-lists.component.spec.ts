import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllShoppingListsComponent } from './all-shopping-lists.component';

describe('AllShoppingListsComponent', () => {
  let component: AllShoppingListsComponent;
  let fixture: ComponentFixture<AllShoppingListsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllShoppingListsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllShoppingListsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
