import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewShoppingListComponent } from './new-shopping-list/new-shopping-list.component';
import { AllShoppingListsComponent } from './all-shopping-lists/all-shopping-lists.component';



const routes: Routes = [
  {path: 'newlist', component: NewShoppingListComponent },
  {path: 'alllists', component: AllShoppingListsComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
